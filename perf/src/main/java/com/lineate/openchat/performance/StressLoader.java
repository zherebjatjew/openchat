package com.lineate.openchat.performance;

import com.lineate.openchat.bot.OpenchatBot;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;
import java.util.stream.IntStream;

/**
 * Stress test application: run thousands bots in parallel and measures elapsed time.
 */
public class StressLoader {
    public static final int N_THREADS = 1000;
    public static final int USER_COUNT = 10000;

    public static void main(String [] args) {
        BasicConfigurator.configure();
        ExecutorService executorService = Executors.newFixedThreadPool(N_THREADS);
        long start = System.currentTimeMillis();
        CompletableFuture<Boolean> [] futures = IntStream.rangeClosed(1, USER_COUNT)
                .mapToObj(i -> "user" + i)
                .map(OpenchatBot::new)
                .map(client -> CompletableFuture.supplyAsync(client::run, executorService))
                .toArray(CompletableFuture[]::new);
        CompletableFuture.allOf(futures).join();
        LoggerFactory.getLogger(StressLoader.class).info("{} users processed in {} ms",
                USER_COUNT, System.currentTimeMillis() - start);
        executorService.shutdown();
    }
}
