package com.lineate.openchat.bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;

/**
 * A simple bot that posts to chat a couple of messages and commands.
 */
public class OpenchatBot {
    private static final Logger LOG = LoggerFactory.getLogger(BotApplication.class);
    private String name;

    public OpenchatBot(String name) {
        this.name = name;
    }

    public boolean run() {
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress("localhost", 3456));
            LOG.info("User {} joined the chat", name);
            if (socket.isConnected()) {
                InputStream input = socket.getInputStream();
                OutputStream output = socket.getOutputStream();
                post(output, name);
                post(output, "\\help");
                post(output, "My first message");
                post(output, "My second message");
                post(output, "\\quit");
                receive(input, socket);
            }
        } catch (IOException e) {
            LOG.info("Terminated", e);
        }
        return true;
    }

    private void receive(InputStream input, Socket socket) throws IOException {
        socket.setSoTimeout(100);
        StringBuilder str = new StringBuilder();
        while (!socket.isClosed()) {
            try {
                byte [] response = new byte[2048];
                int ln = input.read(response);
                if (ln == -1) {
                    break;
                }
                String line = new String(response, 0, ln, Charset.forName("UTF8"));
                str.append(line);
            } catch (SocketTimeoutException e) {
                break;
            }
        }
        LOG.info(str.toString());
    }

    private void post(OutputStream output, String text) throws IOException {
        output.write((text + '\n').getBytes(Charset.forName("UTF8")));
    }
}
