package com.lineate.openchat.bot;

import org.apache.log4j.BasicConfigurator;

/**
 * The application runs single bot.
 */
public class BotApplication {

    public static void main(String [] args) {
        BasicConfigurator.configure();
        new OpenchatBot("new user").run();
    }
}
