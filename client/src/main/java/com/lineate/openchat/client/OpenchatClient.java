package com.lineate.openchat.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Client for the chat. You can run multiple chats in different consoles and make a conversation between them.
 */
public class OpenchatClient {
    public static void main(String [] args) throws IOException {
        String host = args.length > 0 ? args[0] : "localhost";
        try {
            int port = args.length > 1 ? Integer.parseInt(args[1]) : 3456;
            new OpenchatClient().start(host, port);
        } catch (NumberFormatException e) {
            System.err.println("Usage: client [host] [port]");
        }
    }

    private void start(String host, int port) throws IOException {
        Charset charset = Charset.forName("UTF-8");
        try (Socket socket = new Socket()) {
            socket.setSoTimeout(0);
            socket.connect(new InetSocketAddress(host, port));
            ExecutorService executorService = Executors.newFixedThreadPool(2);
            AtomicBoolean goon = new AtomicBoolean(true);
            CompletableFuture<Void> reader = getReader(charset, socket, executorService, goon);
            CompletableFuture<Void> writer = getWriter(charset, socket, executorService, goon);
            CompletableFuture.anyOf(reader, writer).join();
            goon.set(false);
            executorService.shutdown();
        }
    }

    private CompletableFuture<Void> getWriter(Charset charset, Socket socket, ExecutorService executorService,
                                              AtomicBoolean goon) {
        return CompletableFuture.supplyAsync(() -> {
                    try {
                        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
                        while (goon.get()) {
                            String line = consoleReader.readLine();
                            if (line != null) {
                                socket.getOutputStream().write(line.getBytes(charset));
                                if ("\\quit".equals(line)) {
                                    break;
                                }
                            }
                        }
                    } catch (IOException e) {
                        // connection closed
                    } finally {
                        goon.set(false);
                    }
                    System.out.println("");
                    return null;
                }, executorService);
    }

    private CompletableFuture<Void> getReader(Charset charset, Socket socket, ExecutorService executorService,
                                              AtomicBoolean goon) {
        return CompletableFuture.supplyAsync(() -> {
                    try {
                        BufferedReader r = new BufferedReader(new InputStreamReader(socket.getInputStream(), charset));
                        while (goon.get()) {
                            String line = r.readLine();
                            if (line != null) {
                                System.out.println(line);
                            }
                        }
                    } catch (IOException ex) {
                        // connection closed
                    } finally {
                        goon.set(false);
                    }
                    return null;
                }, executorService);
    }
}
