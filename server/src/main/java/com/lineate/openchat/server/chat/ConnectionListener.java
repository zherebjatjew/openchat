package com.lineate.openchat.server.chat;

/**
 * Listens to the opened connections.
 */
public interface ConnectionListener {
    void startListening();
}
