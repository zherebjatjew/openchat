package com.lineate.openchat.server.chat;

import com.lineate.openchat.server.persistence.MessageStorage;
import com.lineate.openchat.server.transport.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Translates messages: commands are sent to execute, plain messages are passed to the other users.
 */
public class MessageParser {
    private static final Logger LOG = LoggerFactory.getLogger(MessageParser.class);

    private final Map<String, Consumer<Connection>> commands;
    private final MessageStorage messageStorage;
    private final ConnectionRegistrar connectionRegistrar;
    private final int recentMessages;

    public MessageParser(MessageStorage messageStorage,
                         ConnectionRegistrar connectionRegistrar, int recentMessages) {
        this.recentMessages = recentMessages;
        this.messageStorage = messageStorage;
        this.connectionRegistrar = connectionRegistrar;
        commands = new HashMap<>();
        registerDefaultCommandSet();
    }

    public void dispatchMessage(Connection connection, String message) {
        Consumer<Connection> command = commands.get(message);
        if (command == null) {
            if (!authorized(connection)) {
                authorize(connection, message);
            } else {
                MessageDTO messageDTO = new MessageDTO(connection.getPayload().toString(), message);
                messageStorage.post(messageDTO);
                broadcast(messageDTO.toString());
                closeTransmission(connection);
            }
        } else {
            LOG.debug("Recognized command {}", message);
            command.accept(connection);
        }
    }

    private void broadcast(String message) {
        connectionRegistrar.broadcast(connection -> connection.write(message));
    }

    private void registerDefaultCommandSet() {
        // quit
        registerCommand("\\quit", cn -> {
            if (cn.getPayload() != null) {
                String author = cn.getPayload().toString();
                LOG.info("User '{}' left", author);
                messageStorage.unregister(author);
                MessageDTO messageDTO = new MessageDTO(author, "*** left the chat ***");
                messageStorage.post(messageDTO);
                connectionRegistrar.removeConnection(cn);
                broadcast(messageDTO.toString());
            }
            cn.close();
        });
        // help
        registerCommand("\\help", cn -> {
            cn.write("\n\\quit\tleave the chat\n\\help\tshow this message");
            closeTransmission(cn);
        });
    }

    public void registerCommand(String command, Consumer<Connection> handler) {
        commands.put(command, handler);
    }

    private void authorize(Connection connection, String loginName) {
        if (!messageStorage.register(loginName)) {
            LOG.info("Attempt to log in with existing name '{}'", loginName);
            connection.write("Sorry, this name is already registered.");
            connectionRegistrar.removeConnection(connection);
        } else {
            LOG.info("User '{}' joined", loginName);
            connection.setPayload(loginName);
            showRecentMessages(connection);
            MessageDTO messageDTO = new MessageDTO(loginName, "*** joined to the chat ***");
            messageStorage.post(messageDTO);
            broadcast(messageDTO.toString());
            closeTransmission(connection);
        }
    }

    private void closeTransmission(Connection connection) {
        connection.write("\n");
    }

    private void showRecentMessages(Connection connection) {
        List<MessageDTO> chatHistory = messageStorage.getLast(recentMessages);
        if (chatHistory.isEmpty()) {
            closeTransmission(connection);
        } else {
            chatHistory.stream()
                    .map(MessageDTO::toString)
                    .forEach(connection::write);
        }
    }

    private boolean authorized(Connection connection) {
        return connection.getPayload() != null;
    }
}
