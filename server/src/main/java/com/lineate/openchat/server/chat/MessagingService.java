package com.lineate.openchat.server.chat;

import java.io.IOException;

/**
 * Main connection manager.
 */
public interface MessagingService {
    /**
     * Accepts connections.
     *
     * @throws IOException
     */
    void start() throws IOException;
}
