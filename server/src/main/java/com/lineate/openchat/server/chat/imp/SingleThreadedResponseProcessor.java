package com.lineate.openchat.server.chat.imp;

import com.lineate.openchat.server.chat.ResponseProcessor;
import com.lineate.openchat.server.transport.Connection;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.BiConsumer;

/**
 * Single-threaded implementation of {@link ResponseProcessor}. Multithreading is overkill here,
 * because {@link SingleThreadedResponseProcessor#processMessage(String, Connection)} is called
 * from multiple threads.
 */
public class SingleThreadedResponseProcessor implements ResponseProcessor {
    private final BiConsumer<Connection, String> handler;

    public SingleThreadedResponseProcessor(BiConsumer<Connection, String> handler) {
        this.handler = handler;
    }

    @Override
    public void processMessage(String content, Connection connection) {
        String [] lines = content.split("\n");
        for (String line : lines) {
            handler.accept(connection, line);
        }
    }
}
