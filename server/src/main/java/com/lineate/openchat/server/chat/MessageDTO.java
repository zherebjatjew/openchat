package com.lineate.openchat.server.chat;

/**
 * Single message.
 */
public class MessageDTO {
    private long unixtime;
    private String author;
    private String content;

    public MessageDTO(String author, String content) {
        unixtime = System.currentTimeMillis();
        this.author = author;
        this.content = content;
    }

    public String toString() {
        // TODO obtain bot-side timezone and use here
        return String.format("[%tF %tT %tz] %s: %s%n", unixtime, unixtime, unixtime, author, content);
    }
}
