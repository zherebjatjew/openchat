package com.lineate.openchat.server.chat.imp;

import com.lineate.openchat.server.chat.ConnectionRegistrar;
import com.lineate.openchat.server.chat.MessagingService;
import com.lineate.openchat.server.transport.Connection;
import com.lineate.openchat.server.transport.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MessagingServiceImpl implements MessagingService {

    private ConnectionFactory connectionFactory;
    private ConnectionRegistrar connectionRegistrar;

    public MessagingServiceImpl(ConnectionFactory connectionFactory, ConnectionRegistrar connectionRegistrar) {
        this.connectionFactory = connectionFactory;
        this.connectionRegistrar = connectionRegistrar;
    }

    @Override
    public void start() {
        while (true) {
            Connection connection = connectionFactory.accept();
            connection.write("Hello! Please type your name\n");
            connectionRegistrar.registerConnection(connection);
        }
    }
}
