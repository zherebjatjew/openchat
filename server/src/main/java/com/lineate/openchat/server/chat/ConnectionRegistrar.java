package com.lineate.openchat.server.chat;

import com.lineate.openchat.server.transport.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Consumer;

/**
 * Manages connection lifecycle.
 */
public class ConnectionRegistrar {
    private static final Logger LOG = LoggerFactory.getLogger(ConnectionRegistrar.class);

    private final Set<Connection> connections = Collections.synchronizedSet(new HashSet<>());

    /**
     * At-most-once implementation of broadcasting.
     *
     * <p>The method could boil down to <code>new HasSet<>(connections).forEach(consumer)</></code>,
     * but this short implementation would miss the connections added in the middle of the cycle.</p>
     *
     * @param consumer the function to call for each connection
     */
    public void broadcast(Consumer<Connection> consumer) {
        Set<Connection> passed = new HashSet<>(connections.size());
        while (true) {
            try {
                connections.stream().parallel().filter(passed::add).forEach(consumer);
                break;
            } catch (ConcurrentModificationException e) {
                LOG.trace("Connection list modified");
            }
        }
    }

    public void registerConnection(Connection connection) {
        connections.add(connection);
    }

    public void removeConnection(Connection connection) {
        connections.remove(connection);
        connection.close();
    }

}
