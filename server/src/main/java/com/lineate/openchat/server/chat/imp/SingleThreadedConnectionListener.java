package com.lineate.openchat.server.chat.imp;

import com.lineate.openchat.server.chat.ConnectionRegistrar;
import com.lineate.openchat.server.chat.ResponseProcessor;
import com.lineate.openchat.server.chat.ConnectionListener;
import com.lineate.openchat.server.transport.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;


/**
 * Listen to multiple channels and process input as soon as it's coming.
 */
public class SingleThreadedConnectionListener implements ConnectionListener {
    private static final Logger LOG = LoggerFactory.getLogger(SingleThreadedConnectionListener.class);

    private ResponseProcessor responseProcessor;
    private ConnectionRegistrar connections;
    private int timeout;

    public SingleThreadedConnectionListener(ResponseProcessor responseProcessor, ConnectionRegistrar connections, int timeout) {
        this.timeout = timeout;
        this.responseProcessor = responseProcessor;
        this.connections = connections;
    }

    @Override
    public void startListening() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            try {
                while (true) {
                    PingConsumer ping = new PingConsumer();
                    connections.broadcast(ping);
                    if (!ping.hadData()) {
                        Thread.sleep(timeout);
                    }
                }
            } catch (InterruptedException e) {
                LOG.info("Terminated");
            }
        });
    }

    private class PingConsumer implements Consumer<Connection> {
        private final AtomicBoolean found = new AtomicBoolean(false);

        /**
         * Reads message from the channel if any. If there was anything to read,
         * the <code>hadData</code> method will return true.
         *
         * @param connection connection to read info from
         */
        @Override
        public void accept(Connection connection) {
            try {
                InputStream stream = connection.getInputStream();
                int available = stream.available();
                if (available > 0) {
                    byte [] buffer = new byte[available];
                    int count = stream.read(buffer);
                    if (count > 0) {
                        String text = new String(buffer, 0, count, connection.getEncoding());
                        responseProcessor.processMessage(text, connection);
                    }
                    found.set(true);
                }
            } catch (IOException | IllegalStateException e) {
                connections.removeConnection(connection);
            } catch (RuntimeException re) {
                connections.removeConnection(connection);
                LOG.warn("Unknown exception", re);
            }
        }

        boolean hadData() {
            return found.get();
        }
    }
}
