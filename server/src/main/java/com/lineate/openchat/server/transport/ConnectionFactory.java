package com.lineate.openchat.server.transport;

/**
 * Generates connections for new clients.
 */
public interface ConnectionFactory {
    Connection accept();
}
