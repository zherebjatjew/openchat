package com.lineate.openchat.server.persistence.imp;

import com.lineate.openchat.server.chat.MessageDTO;
import com.lineate.openchat.server.persistence.MessageStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * In-memory implementation of {@link MessageStorage}. Messages are never removed.
 */
public class InMemoryMessageStorage implements MessageStorage {
    private final List<MessageDTO> messages;
    private final Set<String> users;

    public InMemoryMessageStorage() {
        messages = new ArrayList<>(1000);
        users = new HashSet<>();
    }

    @Override
    public List<MessageDTO> getLast(int count) {
        int total = messages.size();
        int idx = total - count;
        if (idx < 0) {
            idx = 0;
        }
        return messages.subList(idx, total);
    }

    @Override
    public void post(MessageDTO message) {
        messages.add(message);
    }

    @Override
    public boolean register(String login) {
        return users.add(login);
    }

    @Override
    public void unregister(String login) {
        users.remove(login);
    }
}
