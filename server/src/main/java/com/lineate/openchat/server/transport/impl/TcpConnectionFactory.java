package com.lineate.openchat.server.transport.impl;

import com.lineate.openchat.server.transport.Connection;
import com.lineate.openchat.server.transport.ConnectionFactory;

import javax.net.ServerSocketFactory;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 * A TCP implementation of {@code ConnectionFactory}.
 */
public class TcpConnectionFactory implements ConnectionFactory {
    private volatile ServerSocket server;
    private final int port;
    private final Charset encoding;

    public TcpConnectionFactory(int port, Charset encoding) {
        this.port = port;
        this.encoding = encoding;
    }

    @Override
    public Connection accept() {
        try {
            Socket socket = getServer().accept();
            socket.setSoTimeout(0);
            return new TcpConnection(socket, encoding);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private ServerSocket getServer() throws IOException {
        if (server == null) {
            synchronized (this) {
                if (server == null) {
                    server = ServerSocketFactory.getDefault().createServerSocket(port);
                }
            }
        }
        return server;
    }
}
