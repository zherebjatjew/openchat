package com.lineate.openchat.server;

import com.lineate.openchat.server.chat.*;
import com.lineate.openchat.server.chat.imp.SingleThreadedConnectionListener;
import com.lineate.openchat.server.chat.imp.MessagingServiceImpl;
import com.lineate.openchat.server.chat.imp.SingleThreadedResponseProcessor;
import com.lineate.openchat.server.persistence.MessageStorage;
import com.lineate.openchat.server.persistence.imp.InMemoryMessageStorage;
import com.lineate.openchat.server.transport.ConnectionFactory;
import com.lineate.openchat.server.transport.impl.TcpConnectionFactory;
import org.apache.commons.configuration2.CombinedConfiguration;
import org.apache.commons.configuration2.builder.combined.CombinedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;

public class ServerApplication {
    public static void main(String [] args) throws IOException, ConfigurationException {
        new ServerApplication().run(args);
    }

    private void run(String [] args) throws ConfigurationException, IOException {
        BasicConfigurator.configure();
        CombinedConfiguration config = getConfig();
        int port = config.getInt("port");
        Charset encoding = Charset.forName(config.getString("encoding"));
        ConnectionFactory connectionFactory = new TcpConnectionFactory(port, encoding);
        MessageStorage storage = new InMemoryMessageStorage();
        ConnectionRegistrar connectionRegistrar = new ConnectionRegistrar();
        MessageParser dispatcher = new MessageParser(storage, connectionRegistrar,
                config.getInt("recent-message-count"));
        ResponseProcessor processor = new SingleThreadedResponseProcessor(dispatcher::dispatchMessage);
        ConnectionListener connectionListener = new SingleThreadedConnectionListener(
                processor, connectionRegistrar, config.getInt("read-timeout"));
        LoggerFactory.getLogger(getClass()).info("Openchat is listening to port {}", port);
        connectionListener.startListening();
        MessagingService service = new MessagingServiceImpl(connectionFactory, connectionRegistrar);
        service.start();
    }

    private CombinedConfiguration getConfig() throws ConfigurationException {
        Parameters parameters = new Parameters();
        return new CombinedConfigurationBuilder()
                .configure(parameters.fileBased().setURL(
                        getClass().getClassLoader().getResource("properties.config")
                )).getConfiguration();
    }
}
