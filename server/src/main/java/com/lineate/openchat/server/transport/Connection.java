package com.lineate.openchat.server.transport;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

/**
 * Connection to client.
 */
public interface Connection {
    InputStream getInputStream();
    OutputStream getOutputStream();
    void close();
    void write(String text);
    Object getPayload();
    void setPayload(Object value);
    Charset getEncoding();
    void setEncoding(Charset charset);
}
