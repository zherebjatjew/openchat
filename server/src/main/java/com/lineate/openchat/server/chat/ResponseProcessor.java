package com.lineate.openchat.server.chat;

import com.lineate.openchat.server.transport.Connection;

/**
 * Processes messages from connected clients.
 */
public interface ResponseProcessor {
    void processMessage(String content, Connection connection);
}
