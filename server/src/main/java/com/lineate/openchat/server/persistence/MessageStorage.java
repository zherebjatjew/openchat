package com.lineate.openchat.server.persistence;

import com.lineate.openchat.server.chat.MessageDTO;

import java.util.List;

/**
 * Storage for posted messages.
 */
public interface MessageStorage {
    List<MessageDTO> getLast(int count);
    void post(MessageDTO message);
    boolean register(String login);
    void unregister(String login);
}
