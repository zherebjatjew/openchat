package com.lineate.openchat.server.transport.impl;

import com.lineate.openchat.server.transport.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 * A TCP implementation of {@code Connection}.
 */
public class TcpConnection implements Connection {
    private static final Logger LOG = LoggerFactory.getLogger(TcpConnection.class);

    private final Socket socket;
    private Object payload;
    private Charset encoding;

    TcpConnection(Socket socket, Charset encoding) {
        this.socket = socket;
        this.encoding = encoding;
    }

    @Override
    public InputStream getInputStream() {
        if (socket.isClosed()) {
            throw new IllegalStateException("This connection is already closed");
        }
        try {
            return socket.getInputStream();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public OutputStream getOutputStream() {
        if (socket.isClosed()) {
            throw new IllegalStateException("This connection is already closed");
        }
        try {
            return socket.getOutputStream();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            LOG.warn("Error closing socket", e);
        }
    }

    @Override
    public synchronized void write(String text) {
        try {
            getOutputStream().write(text.getBytes(encoding));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Object getPayload() {
        return payload;
    }

    @Override
    public void setPayload(Object value) {
        payload = value;
    }

    @Override
    public Charset getEncoding() {
        return encoding;
    }

    @Override
    public void setEncoding(Charset charset) {
        encoding = charset;
    }
}
