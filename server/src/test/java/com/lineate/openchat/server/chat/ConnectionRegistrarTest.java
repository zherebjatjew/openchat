package com.lineate.openchat.server.chat;

import com.lineate.openchat.server.transport.Connection;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import static org.junit.Assert.*;

public class ConnectionRegistrarTest {
    private ConnectionRegistrar registrar;

    @Before
    public void setup() {
        registrar = new ConnectionRegistrar();
    }
    @Test
    public void shouldBroadcastToAllConnections() {
        registrar.registerConnection(Mockito.mock(Connection.class));
        registrar.registerConnection(Mockito.mock(Connection.class));
        registrar.registerConnection(Mockito.mock(Connection.class));

        AtomicInteger count = new AtomicInteger(0);
        registrar.broadcast(connection -> count.incrementAndGet());

        assertEquals(3, count.get());
    }

    @Test
    public void shouldBroadcastToTheNewlyAddedConnections() {
        registrar.registerConnection(Mockito.mock(Connection.class));
        registrar.registerConnection(Mockito.mock(Connection.class));
        registrar.registerConnection(Mockito.mock(Connection.class));

        AtomicInteger count = new AtomicInteger(0);
        AtomicBoolean added = new AtomicBoolean(false);
        registrar.broadcast(connection -> {
            count.incrementAndGet();
            if (!added.getAndSet(true)) {
                registrar.registerConnection(Mockito.mock(Connection.class));
            }
        });

        assertEquals(4, count.get());
    }

    @Test
    public void shouldRespectOtherThreads() {
        ReentrantLock lock = new ReentrantLock();
        lock.lock();
        Executors.newSingleThreadExecutor().submit(() -> {
            lock.lock();
            try {
                registrar.registerConnection(Mockito.mock(Connection.class));
            } finally {
                lock.unlock();
            }
        });
        registrar.registerConnection(Mockito.mock(Connection.class));
        registrar.registerConnection(Mockito.mock(Connection.class));
        registrar.registerConnection(Mockito.mock(Connection.class));

        AtomicInteger count = new AtomicInteger(0);
        registrar.broadcast(connection -> {
                count.incrementAndGet();
                if (lock.isHeldByCurrentThread()) {
                    try {
                        lock.unlock();
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
        });

        assertEquals(4, count.get());
    }
}