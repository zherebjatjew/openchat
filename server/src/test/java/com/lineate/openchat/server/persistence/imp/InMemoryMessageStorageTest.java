package com.lineate.openchat.server.persistence.imp;


import com.lineate.openchat.server.chat.MessageDTO;
import com.lineate.openchat.server.persistence.MessageStorage;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class InMemoryMessageStorageTest {
    private MessageStorage storage;

    @Before
    public void setup() {
        storage = new InMemoryMessageStorage();

        storage.post(new MessageDTO("xxx", "hi there"));
        storage.post(new MessageDTO("yyy", "hi"));
        storage.post(new MessageDTO("xxx", "whatsup?"));
        storage.post(new MessageDTO("yyy", "get lost"));
        storage.post(new MessageDTO("xxx", "suit yourself"));
    }

    @Test
    public void shouldGetMessages() {
        List<MessageDTO> messages = storage.getLast(2);
        assertEquals(2, messages.size());
    }

    @Test
    public void shouldGetAllIfTooMuchWasQuieried() {
        List<MessageDTO> messages = storage.getLast(100000);
        assertEquals(5, messages.size());
    }

    @Test
    public void shouldStoreMessage() {
        storage.post(new MessageDTO("me", "my message"));
        List<MessageDTO> messages = storage.getLast(1);
        String str = messages.get(0).toString();
        assertTrue(Pattern.compile(".* me: my message\n").matcher(str).matches());
    }
}