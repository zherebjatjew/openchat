package com.lineate.openchat.server.chat;

import org.junit.Test;

import java.util.regex.Pattern;

import static org.junit.Assert.assertTrue;

public class MessageDTOTest {

    @Test
    public void testToString() {
        MessageDTO msg = new MessageDTO("author", "some text");

        String str = msg.toString();

        Pattern pattern = Pattern.compile("\\[\\d{4}-\\d\\d-\\d\\d \\d\\d:\\d\\d:\\d\\d [+-]\\d{1,4}] author: some text\n");
        assertTrue(pattern.matcher(str).matches());
    }
}