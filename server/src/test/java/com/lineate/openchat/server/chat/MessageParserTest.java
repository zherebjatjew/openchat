package com.lineate.openchat.server.chat;


import com.lineate.openchat.server.persistence.MessageStorage;
import com.lineate.openchat.server.transport.Connection;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MessageParserTest {
    private MessageStorage storage = Mockito.mock(MessageStorage.class);
    private ConnectionRegistrar registrar = Mockito.mock(ConnectionRegistrar.class);
    private MessageParser dispatcher = new MessageParser(storage, registrar, 10);

    @Test
    public void shouldRecognizeHelpCommand() {
        Connection connection = Mockito.mock(Connection.class);

        dispatcher.dispatchMessage(connection, "\\help");

        Mockito.verify(storage, Mockito.never()).post(Mockito.any());
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(connection, Mockito.times(2)).write(captor.capture());
        assertTrue(Pattern.compile("leave the chat").matcher(captor.getAllValues().get(0)).find());
    }

    @Test
    public void shouldRecognizeQuitCommand() {
        Connection connection = Mockito.mock(Connection.class);
        Mockito.when(connection.getPayload()).thenReturn("");

        dispatcher.dispatchMessage(connection, "\\quit");

        Mockito.verify(connection).close();
        Mockito.verify(storage).unregister(Mockito.anyString());
    }

    @Test
    public void shouldRecognizeACustomCommand() {
        dispatcher.registerCommand("test", cn -> cn.write("1234"));
        Connection connection = Mockito.mock(Connection.class);

        dispatcher.dispatchMessage(connection, "test");

        Mockito.verify(storage, Mockito.never()).post(Mockito.any());
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(connection).write(captor.capture());
        assertEquals("1234", captor.getValue());
    }

    @Test
    public void shouldTreatFirstMessageAsUserName() {
        String login = "test";
        Connection connection = Mockito.mock(Connection.class);
        Mockito.when(storage.register(login)).thenReturn(Boolean.TRUE);

        dispatcher.dispatchMessage(connection, login);

        Mockito.verify(connection, Mockito.times(2)).write(Mockito.anyString());
        ArgumentCaptor<MessageDTO> msgCaptor = ArgumentCaptor.forClass(MessageDTO.class);
        Mockito.verify(storage).post(msgCaptor.capture());
        String capturedMessage = msgCaptor.getValue().toString();
        assertTrue(Pattern.compile(login + ".*joined to the chat").matcher(capturedMessage).find());
        ArgumentCaptor<String> nameCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(connection).setPayload(nameCaptor.capture());
        assertEquals(login, nameCaptor.getValue());
    }

    @Test
    public void shouldStoreMessage() {
        Connection connection = Mockito.mock(Connection.class);
        String login = "John Doe";
        String message = "Hi all!";
        Mockito.when(connection.getPayload()).thenReturn(login);

        dispatcher.dispatchMessage(connection, message);

        ArgumentCaptor<MessageDTO> captor = ArgumentCaptor.forClass(MessageDTO.class);
        Mockito.verify(storage).post(captor.capture());
        assertTrue(Pattern.compile(login + ": " + message + "\n$").matcher(captor.getValue().toString()).find());
    }

    @Test
    public void shouldRejectDuplicateLogin() {
        String login = "John Doe";
        Connection connection = Mockito.mock(Connection.class);
        Mockito.when(storage.register(login)).thenReturn(Boolean.FALSE);

        dispatcher.dispatchMessage(connection, login);

        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(connection).write(captor.capture());
        assertEquals("Sorry, this name is already registered.", captor.getValue());
    }
}