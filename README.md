# Multi-user console chat

## Author

[Dmitry Zherebyatev](mailto:zherebjatjew@gmail.com) 2019
for [miro](https://miro.com)

## How to run

```
mvn install
cd server/target
java -jar server-0.1-jar-with-dependencies.jar

# each client is to run in another console
cd client/target
java -jar client-0.1-jar-with-dependencies.jar

# performance tests
cd perf/target
java -jar perf-0.1-jar-with-dependencies.jar
```
## Commands
* \help   show command list
* \quit   leave the chat

## Release notes
### 0.1
The performance tests are dirty. I wanted to use jmeter, but then went for a simpler solution.
Anyway, it is not serious to check performance using only one machine.
However, these tests have disclosured some issues.